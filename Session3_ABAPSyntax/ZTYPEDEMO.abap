*&---------------------------------------------------------------------*
*& Report ZHALLOPERSON
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT ztypedemo.

"Definition von Datenobjekten mit verschiedenen Typen
DATA zweiZeichen TYPE c LENGTH 2.
DATA einDatum TYPE d.
DATA eineKommazahl TYPE p DECIMALS 2.
DATA nochEineKommazahl TYPE f.
DATA eineGanzzahl TYPE i.
DATA einBoolean TYPE abap_bool.

"Zuweisungen von Literalen
zweiZeichen = 'AT'.
einDatum = '19800417'.
eineKommazahl = '0.5'.
nocheinekommazahl = '-0.05'.
eineganzzahl = 0.
einboolean = abap_false.

"Ausgabe mit Kettensatz
WRITE:
  'CHAR: ',zweizeichen, /,
  'DATUM: ',einDatum, /,
  'FIXKOMMA: ',eineKommazahl, /,
  'GLEITKOMMA: ',nocheinekommazahl, /,
  'GANZZAHL: ',eineganzzahl, /,
  'WAHRHEITSWERT: ',einboolean.