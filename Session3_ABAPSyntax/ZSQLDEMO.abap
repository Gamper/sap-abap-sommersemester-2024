*&---------------------------------------------------------------------*
*& Report ZSQLDEMO
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zsqldemo.


TABLES saplane.

"Datenobjekt vom Typ der Tabelle saplane (ist damit eine Struktur, die als Komponenten alle Spalten der Tabelle enthält)
DATA einflugzeug TYPE saplane.

DATA vieleflugzeuge TYPE TABLE OF saplane.

"PARAMETER als User-Eingabe definieren
"Siehe Scanner in Java
PARAMETERS p_brand TYPE saplane-producer.

"von-bis-Eingabe
SELECT-OPTIONS so_seats FOR saplane-seatsmax.



"Spaltenbeschriftungen
WRITE: text-001, text-002, /.

"OpenSQL zum einfachen Durchlaufen einer Datenbanktabelle
SELECT * FROM saplane INTO einflugzeug WHERE producer = p_brand AND seatsmax IN so_seats.
  WRITE: /, einflugzeug-planetype UNDER text-001, einflugzeug-producer UNDER text-002.
ENDSELECT.

"OpenSQL zum Speichern aller Tabellenzeilen in einen interne Tabelle=Array/ArrayList
SELECT * FROM saplane INTO TABLE vieleflugzeuge.