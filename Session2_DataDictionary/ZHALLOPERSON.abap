*&---------------------------------------------------------------------*
*& Report ZHALLOPERSON
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zhalloperson.

"Definition eines Datenobjekts vorname mit dem primitiven Typ string
DATA vorname TYPE string.

"Zuweisung eines String-Literals
vorname = 'Michael'.


"Datenobjekt person vom Typ zperson (eine Struktur)
DATA person TYPE zperson.


"Zuweisung an die Struktur-Komponenten
person-vorname = 'Michael'.
person-nachname = 'Gamper'.
person-titel = 'Meisterzauberer'.

"Ausgabe der einzelnen Komponenten im Report
WRITE person-titel.
WRITE person-vorname.
WRITE person-nachname.