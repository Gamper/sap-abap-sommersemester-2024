*&---------------------------------------------------------------------*
*& Report ZINTERNTABLEDEMO
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zinterntabledemo.

TABLES sflight.

"Definition des Typs ZPERSON als Strukur.
"Der Typ kann hier im Programm definiert werden (programmlokal), oder
"oder im Dictionary definiert und hier verwendet werden (wiederverwendbar)

TYPES: BEGIN OF tperson,
         id        TYPE c LENGTH 4,
         title     TYPE c LENGTH 50,
         firstname TYPE c LENGTH 255,
         lastname  TYPE c LENGTH 255,
       END OF tperson.

"Alternativ, falls Dictionary-Typen verwendet werden sollen:
*TYPES: BEGIN OF tperson,
*         id        LIKE sflight-connid, "Verwendet denselben Typ wie die Spalte connid in der Tabelle sflight
*         title     TYPE ztitel, "Verwendet das Datenelement ZTITEL aus dem Dictionary
*         firstname TYPE zname, " Verwendet das Datenelement ZNAME aus dem Dictionary
*         lastname  TYPE zname,
*       END OF tperson.

"Definiere eine interne Tabelle
"Entspricht einer ArrayList von Typ Person in Java

"Eine Standard-Tabelle ohne irgendwelche Keys. Entspricht einem normalen Array of Objects. Jede "Spalte" ist gleichberechtigt.
"Langsame Suche, Zugriff über Index ist möglich.
*DATA it_personen TYPE STANDARD TABLE OF tperson.

"Alternativ kann auch eine Dictionary-Struktur verwendet werden, etwa
*DATA it_personen TYPE STANDARD TABLE OF zperson.

"Ein paar VARIANTEN
"Eine Standard-Tabelle mit definition eines Suchdatenstruktur für die Komponente id.
"Schnelle Suche nach id, Zugriff über Index und Key möglich
DATA it_personen TYPE STANDARD TABLE OF tperson WITH UNIQUE SORTED KEY pk_id COMPONENTS id.

"Eine Sortierte Tabelle. Sortiert wird nach der Komponente id.
"Schnelle Suche nach id, Zugriff über Index und Key möglich. Umsortieren nicht möglich, Nur Append erlaubt, kein Insert.
* DATA it_personen TYPE SORTED TABLE OF tperson WITH UNIQUE KEY id.



"Definiere eine Work Area
"Entspricht einer (Lauf-)Variable vom Typ Person in Java
DATA wa_person TYPE tperson.
DATA wa_person1 TYPE tperson.
DATA wa_person2 TYPE tperson.
DATA wa_person3 TYPE tperson.


"DATENVERARBEITUNG

"Lege eine Person an:
wa_person1-id = 'SUNT'.
wa_person1-firstname = 'Sepp'.
wa_person1-lastname = 'Unterwurzacher'.

"Lege noch eine Person an:
wa_person2-id = 'GAMP'.
wa_person2-title = 'MZ'.
wa_person2-firstname = 'Michael'.
wa_person2-lastname = 'Gamper'.

"Lege noch eine Person an:
"Beachte: ID muss nicht eindeutig sein, wenn Standard Table ohne Keys.
wa_person3-id = 'EVIL'.
wa_person3-title = 'DR'.
wa_person3-firstname = ''.
wa_person3-lastname = 'EVIL'.



"Füge eine Person der internen Tabelle hinzu (an letzter Stelle)
APPEND wa_person1 TO it_personen.

"Füge eine Person an erster Stelle ein.
INSERT wa_person2 INTO it_personen INDEX 1.

"Füge die dritte Person hinzu
APPEND wa_person3 TO it_personen.
IF sy-subrc <> 0.
  EXIT.
ENDIF.


"DATENAUSGABE aller Personen in der internen Tabelle
WRITE: 'Person Nr: ', 'Kuerzel', 'Vorname', 'Nachname', /.

"Durchlaufe alle Zeilen der internen Tabelle
LOOP AT it_personen INTO wa_person.
  WRITE: /,
    sy-tabix UNDER 'Person Nr: ',
    wa_person-id UNDER 'Kuerzel',
    wa_person-firstname UNDER 'Vorname',
    wa_person-lastname UNDER 'Nachname'.
ENDLOOP.

"Suche nach dem ersten Eintrag
READ TABLE it_personen INTO wa_person INDEX 1.
WRITE: /,'Index 1 gefunden:',/
    'Person Nr: ', 'Kuerzel', 'Vorname', 'Nachname', /,
    wa_person-id UNDER 'Kuerzel',
    wa_person-firstname UNDER 'Vorname',
    wa_person-lastname UNDER 'Nachname'.

"Suche nach dem Eintrag GAMP. Klappt nur, wenn ein KEY definiert wurde
READ TABLE it_personen INTO wa_person WITH TABLE KEY pk_id COMPONENTS id = 'GAMP'.
WRITE: /,'GAMP gefunden:',/
    'Person Nr: ', 'Kuerzel', 'Vorname', 'Nachname', /,
    wa_person-id UNDER 'Kuerzel',
    wa_person-firstname UNDER 'Vorname',
    wa_person-lastname UNDER 'Nachname'.

"Lösche GAMP
DELETE it_personen WHERE id = 'GAMP'.
IF sy-subrc = 0.
  WRITE: /, 'GAMP gelöscht'.
ENDIF.