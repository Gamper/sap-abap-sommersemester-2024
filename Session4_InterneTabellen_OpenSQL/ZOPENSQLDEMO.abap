*&---------------------------------------------------------------------*
*& Report ZSQLDEMO
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zsqldemo2.


TABLES: sflight, spfli.

"Datenobjekt vom Typ der Tabelle sflight
DATA gv_einFlug TYPE sflight.

"Datenobjekt vom Typ interne Tabelle of sflight.
DATA it_fluege TYPE TABLE OF sflight.
DATA wa_flug TYPE sflight.

"PARAMETER als User-Eingabe definieren
*PARAMETERS p_fldate TYPE sflight-fldate.

"SELECT OPTION als User-Eingabe definieren. von-bis-Eingabe
*SELECT-OPTIONS so_price FOR sflight-price.



"ALTE VERSION:
"OpenSQL zum einfachen Durchlaufen einer Datenbanktabelle. Kommt ohne interne Tabelle aus.
*SELECT * FROM sflight INTO gv_einflug.
* WRITE: /,
*    gv_einflug-fldate UNDER text-001,
*    gv_einflug-price LEFT-JUSTIFIED UNDER text-002,
*    gv_einflug-currency UNDER text-003,
*    gv_einflug-planetype  UNDER text-004,
*    gv_einflug-seatsmax  UNDER text-005.
*ENDSELECT.



"DATENBESCHAFFUNG
SELECT * FROM sflight INTO TABLE it_fluege.

"DATENBEARBEITUNG
*DELETE it_fluege WHERE fldate < SY-datum.
"...


"DATENAUSGABE

"Spaltenbeschriftungen
WRITE: text-001, text-002, text-003, text-004, text-005, /.

LOOP AT it_fluege INTO gv_einflug.
  WRITE: /,
    gv_einflug-fldate UNDER text-001,
    gv_einflug-price LEFT-JUSTIFIED UNDER text-002,
    gv_einflug-currency UNDER text-003,
    gv_einflug-planetype  UNDER text-004,
    gv_einflug-seatsmax  UNDER text-005.
ENDLOOP.