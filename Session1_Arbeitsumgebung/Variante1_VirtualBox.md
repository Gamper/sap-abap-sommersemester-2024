# SAP ABAP Trial on OpenSuse for VirtualBox

<!-- ---
title: Anleitung für die Installation einer lokalen Übungsumgebung für SAP ABAP
author: Michael Gamper
date: Februar 2024
--- -->

Diese Anleitung beschreibt, wie man sich eine vollwertige lokale SAP-Entwicklungsumgebung mit Hilfe von VirtualBox einrichtet. Die Anleitung bezieht sich auf Windows als Host-System, kann aber relativ einfach auf MacOS(x86) oder Linux umgelegt werden.

## Systemvoraussetzungen

* 2 CPU-Kerne für den Betrieb der VM
* 6 GB RAM für den Betrieb der VM
* 60 GB Festplattenplatz (für die kopierte virtuelle Maschine). 
* Akutelles [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

Womöglich gibt es bei parallel installiertem Hyper-V / WSL Probleme beim Ausführen von VirtualBox-VMs. Hier kann die Deaktivierung von HyperV/WSL helfen (nur ratsam falls diese Features nicht benötigt werden). Als Kompromiss können neben HyperV auch andere Hypervisor erlaubt werden, allerdings mit Performance-Einbußen:
![Windows Features anpassen](img/windowsfeatures.png)

## Hinzufügen der Virtuellen Maschine in VirtualBox

Kopieren Sie die Dateien der fertigen virtuellen Maschine in ein Verzeichnis. Dieses Verzeichnis sollte für die Dauer des Kurses nicht mehr verschoben werden und auch in keinem Speichercloud-Verzeichnis liegen.

Sie können die virtuelle Maschine einfach hinzufügen, mit STRG-A: 
![VM hinzufügen](img/vmhinzufuegen.png)

Bitte behalten Sie alle Einstellungen der Maschine bei, vor allem die Netzwerkeinstellungen. Gegebenenfalls können Sie mehr CPU-Kerne und mehr RAM vergeben.

## VM starten

Nach dem Starten der VM wird OpenSuse als Serverbetriebssystem hochgefahren. Sie können sich als Linux-Admin einloggen:
* User: root
* PW: testitest

Bitte verändern Sie die Maschine nicht. Installieren Sie auch keine OS-Updates.

## SAP-Stack starten

Nach Eingabe dieser Befehle wird der SAP-Stack gestartet. Das kann einige Minuten dauern. Mitgestartet werden:

* SAP Netweaver (Applikationsserver)
* SAP SyBASE (Datenbankserver)
* SAP Webserver

````bash
su npladm
startsap ALL
````

Nach erfolgreichem Start sollte die Ausgabe etwa so aussehen:

![Erfolgreicher SAP Start](img/sapstackstart.png)


## SAP-Stack und VM herunterfahren

Da der Stack und vor allem die Datenbank einige Zeit benötigt um Daten vom RAM in Dateien zu schreiben, ist ein sauberes Herunterfahren wichtig.

````bash
stopsap ALL
````

Es ist wichtig, die Zeit bis zum Herunterfahren des Stacks abzuwarten, um Datenverlust und inkosistente Zustände zu vermeiden. Danach kann Linux sauber heruntergefahren werden:

````bash
exit
poweroff
````


## Verbindung mit dem SAP Client
![Verbindungseinstellung im SAP Client](img/sapclient_connectionToVBox.png)

### Zugangsdaten
* Benutzername: DEVELOPER
* Passwort: Down1oad

* SAP Admin: SAP*
* Passwort: Down1oad

## Erneuern der Developer-Lizenz (falls abgelaufen)
+ Einloggen mit SAP*-Benutzer
+ Transactionscode: SLICENSE
+ Kopieren der Hardware-ID
+ Ausfüllen des Formulars [MiniSAP Lizenzen](https://go.support.sap.com/minisap/#/minisap): A4H - SAP NetWeaver AS ABAP 7.4 and above (Linux / SAP HANA)
+ Speichern der neuen Lizenz
+ In SLICENSE: Installieren der neuen Lizenz

![Formular zur Beantragung der Lizenz](img/minisap_lizenzen_NPL.png)

Nach dem Löschen der alten Lizenz, kann die neue installiert werden:
![Installieren der neuen Lizenz](img/slicencse_NPL.png)


## Installieren von Eclipse mit ABAP Developer Tools (ADT)

Laut Anleitung auf [developer.sap.com](https://developers.sap.com/tutorials/abap-install-adt.html)

## Weiterführende Links zu Anleitungen, Material, Kurse, Zertifizierungen

Features dieser Installtion auf [community.sap.com](https://community.sap.com/t5/application-development-blog-posts/abap-platform-trial-1909-available-now/ba-p/13567855)
[SAP Tutorial Navigator](https://developers.sap.com/tutorial-navigator.html)
[learning.sap.com](https://learning.sap.com/)
[Learning Hub](https://www.sap.com/training-certification/learning-hub.html)
[Offizielles SAP Training und Zertifizierung](https://training.sap.com/training-course-schedule)
[ABAP Git Support](https://github.com/abapGit/abapGit)