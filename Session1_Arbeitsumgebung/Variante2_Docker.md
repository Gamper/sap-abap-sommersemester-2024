# SAP ABAP Trial on SAP HANA for Docker

<!-- ---
title: Anleitung für die Installation einer lokalen Übungsumgebung für SAP ABAP
author: Michael Gamper
date: Februar 2024
--- -->

Diese Anleitung beschreibt, wie man sich eine vollwertige lokale SAP-Entwicklungsumgebung mit Hilfe von Docker einrichtet. Die Anleitung bezieht sich auf Windows als Host-System, kann aber relativ einfach auf MacOS oder Linux umgelegt werden.

## Systemvoraussetzungen

* 4 CPU-Kerne für Docker
* 16 GB RAM für Docker
* 170 GB Festplattenplatz (Auf der Partition, die Docker benutzt. Für Windows ist das die Partition mit dem WSL2-Linux-Image mit dem Namen ext4.vhdx, meist C:). 

Falls nötig, hier eine Anleitung zum Verschieben des WSL2-Volumes auf [stackoverflow.com](https://stackoverflow.com/questions/62441307/how-can-i-change-the-location-of-docker-images-when-using-docker-desktop-on-wsl2).

## Variante 1: Importieren einer Image-Sicherung
Diese Variante bietet sich an, falls Sie das Image über den Kursleiter bezogen haben. Sie sparen sich dadurch den zeitintensiven Download vom Docker Hub.

````terminal
docker load -i abap_platform_dockerimage.tar
````

Hinweis: Diese Variante funktioniert ausschließlich unter Windows mit Docker auf WSL2-Basis.

## Variante 2: Download über Docker Hub

Allgemeine Anleitung auf [Docker Hub](https://hub.docker.com/r/sapse/abap-platform-trial/).

Hier die Kurzversion: 

````terminal
docker pull sapse/abap-platform-trial:1909_SP01
````

## Container erstellen und erstmalig starten

Dieser Befehl erstellt aus dem Image einen Container mit dem Namen *a4h* und leitet die benötigten Ports weiter. 

Der SAP-Stack sollte nun automatisch starten und nach einigen Minuten zur Verfügung stehen.

````terminal
docker run --stop-timeout 3600 -i --name a4h -h vhcala4hci -p 3200:3200 -p 3300:3300 -p 8443:8443 -p 30213:30213 -p 50000:50000 -p 50001:50001 sapse/abap-platform-trial:1909_SP01 -skip-limits-check -agree-to-sap-license
````

## SAP und Container beenden
Da der Stack und vor allem die HANA-Datenbank einige Zeit benötigt um Daten vom RAM in Dateien zu schreiben, ist ein sauberes Herunterfahren wichtig. Dies kann entweder mit einem Signal an den Server erfolgen:

````terminal
STRG+C
````
oder der Container wird heruntergefahren, mit:

````terminal
docker stop -t 7200 a4h
````

In beiden Fällen ist es wichtig, die Zeit bis zum Herunterfahren des Stacks abzuwarten, um Datenverlust und inkosistente Zustände zu vermeiden.

Auf keinen Fall sollte der Container gelöscht werden, dies führt zum Verlust aller persistierter Daten. Es empfiehlt sich, die Programmme auch außerhalb der SAP-Umgebung zu sichern.

## Container und SAP starten 
Der Container mit dem Namen *a4h* bleibt bestehen und kann jederzeit gestartet werden.

````terminal
docker start a4h
````

## Verbindung mit dem SAP Client
![Verbindungseinstellung im SAP Client](img/sapclient_connectionToLocalSAPonDocker.png)

## Zugangsdaten
* Benutzername: DEVELOPER
* Passwort: **ABAPtr1909**

* SAP Admin: SAP*
* Passwort: ABAPtr1909

## Erneuern der Developer-Lizenz
+ Einloggen mit SAP*-Benutzer
+ Transactionscode: SLICENSE
+ Kopieren der Hardware-ID
+ Ausfüllen des Formulars [MiniSAP Lizenzen](https://go.support.sap.com/minisap/#/minisap): A4H - SAP NetWeaver AS ABAP 7.4 and above (Linux / SAP HANA)
+ Speichern der neuen Lizenz
+ In SLICENSE: Installieren der neuen Lizenz

![Formular zur Beantragung der Lizenz](img/minisap_lizenzen.png)
![Installieren der neuen Lizenz](img/slicencse.png)


## Installieren von Eclipse mit ABAP Developer Tools (ADT)

Laut Anleitung auf [developer.sap.com](https://developers.sap.com/tutorials/abap-install-adt.html)

## Weiterführende Links zu Anleitungen, Material, Kurse, Zertifizierungen

Features dieser Installtion auf [community.sap.com](https://community.sap.com/t5/application-development-blog-posts/abap-platform-trial-1909-available-now/ba-p/13567855)
[SAP Tutorial Navigator](https://developers.sap.com/tutorial-navigator.html)
[learning.sap.com](https://learning.sap.com/)
[Learning Hub](https://www.sap.com/training-certification/learning-hub.html)
[Offizielles SAP Training und Zertifizierung](https://training.sap.com/training-course-schedule)
[ABAP Git Support](https://github.com/abapGit/abapGit)